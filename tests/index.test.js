const {
  logDailyName,
  writeToLogFile,
  stdoutWrite,
  stderrWrite
} = require("../index");

test("write file", () => {
  const fecha = new Date().toLocaleDateString().replace(/\//g, "_");

  const dirname = logDailyName(__dirname, "logs");
  expect(dirname).toBe(__dirname + "/logs_" + fecha + ".log");
  expect(writeToLogFile("test", dirname)).toBe("test");
  expect(stdoutWrite(dirname)).toEqual(undefined);
  expect(stderrWrite(dirname)).toBe(undefined);
});
