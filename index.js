const fs = require("fs");
const _fecha = new Date();

const logDailyName = (_dname, _prefix) => {
  const _date = _fecha.toLocaleDateString().replace(/\//g, "_");

  return `${_dname}/${_prefix}_${_date}.log`;
};

const writeToLogFile = (_originalMsg, _fileName) => {
  const _timestamp = `${("00" + _fecha.getDate()).slice(-2)}/${(
    "00" +
    (_fecha.getMonth() + 1)
  ).slice(-2)}/${_fecha.getFullYear()} ${("00" + _fecha.getHours()).slice(
    -2
  )}:${("00" + _fecha.getMinutes()).slice(-2)}:${(
    "00" + _fecha.getSeconds()
  ).slice(-2)}`;

  fs.appendFileSync(_fileName, `${_timestamp}\t${_originalMsg}\n\n`);

  return _originalMsg;
};

const stdoutWrite = (erroLog) => {
  // //stdout logging hook
  const stdoutWrite0 = process.stdout.write;
  process.stdout.write = (args) => {
    // On stdout write
    writeToLogFile(args, erroLog); // Write to local log file
    args = Array.isArray(args) ? args : [args]; // Pass only as array to prevent internal TypeError for arguments

    return stdoutWrite0.apply(process.stdout, args);
  };
};

const stderrWrite = (erroLog) => {
  //  // stderr logging hook
  const stderrWrite0 = process.stderr.write;
  process.stderr.write = (args) => {
    // On stderr write
    writeToLogFile(args, erroLog); // Write to local error file
    args = Array.isArray(args) ? args : [args]; // Pass only as array to prevent internal TypeError for arguments

    return stderrWrite0.apply(process.stderr, args);
  };
};

module.exports = { logDailyName, writeToLogFile, stdoutWrite, stderrWrite };
